package controller

import (
	"fmt"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/labstack/echo/v4"
	"kanji/db"
	"kanji/model"
	"kanji/request"
	"kanji/response"
	"log"
	"time"
)

type Wordtest struct{}

func WordtestComposer() Wordtest {
	return Wordtest{}
}

type wordtestPageReq struct {
	request.Paging
}

// @Tags Wordtest
// @Summary Page Wordtest
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query wordtestPageReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=response.Pagination{list=[]model.PublicWordtest}} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /wordtest [post]
func (h Wordtest) Page(c echo.Context) error {
	var err error

	req := new(wordtestPageReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	err, cnt, list := wordtestPageData(req)
	if err != nil {
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, response.PayloadPagination(req, list, cnt)).SendJSON(c)
}

func wordtestPageData(req *wordtestPageReq) (error, int, []model.PublicWordtest) {
	var err error
	var cnt int
	var list []model.PublicWordtest
	var count struct{ Count int }

	qSelect := model.GetWordtestQuery().Where()
	qCount := db.Query(`SELECT COUNT(*) from public.wordtest`).Where()

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	err = pgxscan.Get(ctx, conn, &count, qCount.Build(), qCount.Params()...)
	if err != nil {
		return err, cnt, list
	}

	cnt = count.Count

	if req.GetPage() < 1 {
		req.SetPage(1)
	}

	qSelect.OffsetLimit(req.GetOffset(), req.GetLimit())
	rows, err := conn.Query(ctx, qSelect.Build(), qSelect.Params()...)
	//err = pgxscan.Select(ctx, conn, &list, qSelect.Build(), qSelect.Params()...)
	if err != nil {
		fmt.Println("err ", err.Error())
		return err, cnt, list
	}

	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			fmt.Println("rows.Next() ", err.Error())
			return err, cnt, list
		}
		WordtestID := values[0].(int64)
		Wordtest := values[1].(string)
		WordTag := values[2].(string)
		CreateBy := values[3].(int64)
		CreateDt := values[4].(time.Time)
		UpdateBy := values[5].(int64)
		UpdateDt := values[6].(time.Time)

		log.Println(WordtestID, Wordtest, WordTag, CreateBy, CreateDt, UpdateBy, UpdateDt)
	}

	if len(list) == 0 {
		list = make([]model.PublicWordtest, 0)
	}

	return err, cnt, list
}
