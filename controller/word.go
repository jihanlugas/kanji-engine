package controller

import (
	"context"
	"errors"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	"kanji/constant"
	"kanji/db"
	"kanji/model"
	"kanji/request"
	"kanji/response"
	"kanji/validator"
	"net/url"
	"strconv"
)

type Word struct{}

func WordComposer() Word {
	return Word{}
}

type wordPageReq struct {
	request.Paging
}

type wordCreateReq struct {
	Word         string   `db:"word,use_zero" json:"word" form:"word" validate:"required,notexists=word word"`
	WordTag      string   `db:"word_tag,array,use_zero" json:"wordTag" form:"wordTag" validate:"required,oneof='OTHER' 'JLPT_N1_VOCAB' 'JLPT_N2_VOCAB' 'JLPT_N3_VOCAB' 'JLPT_N4_VOCAB' 'JLPT_N5_VOCAB'"`
	ListWordRuby []string `db:"list_word_ruby,array,use_zero" json:"listWordRuby" form:"listWordRuby" validate:""`
	ListMeaning  []string `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" validate:"required"`
	IsSingleruby bool     `db:"is_singleruby,use_zero" json:"isSingleruby" form:"isSingleruby" validate:""`
	IsCommon     bool     `db:"is_common,use_zero" json:"isCommon" form:"isCommon" validate:""`
}

type wordUpdateReq struct {
	WordID       int64    `json:"wordId" form:"wordId" query:"wordId" validate:"required,existsdata=word_id wordId"`
	Word         string   `db:"word,use_zero" json:"word" form:"word" validate:"required"`
	WordTag      string   `db:"word_tag,array,use_zero" json:"wordTag" form:"wordTag" validate:"required,oneof='OTHER' 'JLPT_N1_VOCAB' 'JLPT_N2_VOCAB' 'JLPT_N3_VOCAB' 'JLPT_N4_VOCAB' 'JLPT_N5_VOCAB'"`
	ListWordRuby []string `db:"list_word_ruby,array,use_zero" json:"listWordRuby" form:"listWordRuby" validate:""`
	ListMeaning  []string `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" validate:"required"`
	IsSingleruby bool     `db:"is_singleruby,use_zero" json:"isSingleruby" form:"isSingleruby" validate:""`
	IsCommon     bool     `db:"is_common,use_zero" json:"isCommon" form:"isCommon" validate:""`
}

// @Tags Word
// @Summary Page Word
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query wordPageReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=response.Pagination{list=[]model.WordRes}} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word [post]
func (h Word) Page(c echo.Context) error {
	var err error

	req := new(wordPageReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	err, cnt, list := wordPageData(req)
	if err != nil {
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, response.PayloadPagination(req, list, cnt)).SendJSON(c)
}

// @Tags Word
// @Summary get kanji by kanji
// @Accept json
// @Produce json
// @in header
// @Param kanji path string true "kanji" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.WordRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/word/{word} [get]
func (h Word) GetByWord(c echo.Context) error {
	var err error
	var word model.PublicWord

	reqWord, err := url.QueryUnescape(c.Param("word"))
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	word.Word = reqWord
	err = word.GetByWord(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, word.Res()).SendJSON(c)
}

// @Tags Word
// @Summary get word by id
// @Accept json
// @Produce json
// @in header
// @Param kanji path number true "word_id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.WordRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/id/{word_id} [get]
func (h Word) GetById(c echo.Context) error {
	var err error
	var word model.PublicWord

	ID, err := strconv.ParseInt(c.Param("word_id"), 10, 64)
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	word.WordID = ID
	err = word.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, word.Res()).SendJSON(c)
}

// @Tags Word
// @Summary get word by id
// @Accept json
// @Produce json
// @in header
// @Param kanji path number true "word_id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.WordResDetail} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/id/detail/{word_id} [get]
func (h Word) GetByIdDetail(c echo.Context) error {
	var err error
	var word model.PublicWord

	ID, err := strconv.ParseInt(c.Param("word_id"), 10, 64)
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	word.WordID = ID
	err = word.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return h.getWordDetail(c, conn, ctx, word)
}

// @Tags Word
// @Summary get word by id
// @Accept json
// @Produce json
// @in header
// @Param kanji path string true "word" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.WordResDetail} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/word/detail/{word} [get]
func (h Word) GetByWordDetail(c echo.Context) error {
	var err error
	var word model.PublicWord

	reqWord := c.Param("word_id")
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	word.Word = reqWord
	err = word.GetByWord(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return h.getWordDetail(c, conn, ctx, word)
}

func (h Word) getWordDetail(c echo.Context, conn *pgxpool.Conn, ctx context.Context, word model.PublicWord) error {

	return response.Success(response.ResponseSuccess, word.ResDetail()).SendJSON(c)
}

// @Tags Word
// @Summary Create Word
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body wordCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=model.WordRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/create [post]
func (h Word) Create(c echo.Context) error {
	var err error
	var word model.PublicWord
	var kanji model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(wordCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	listKanji := make([]string, 0)
	chars := []rune(req.Word)
	for i := 0; i < len(chars); i++ {
		if validator.RegKanji.MatchString(string(chars[i])) {
			listKanji = append(listKanji, string(chars[i]))
		}
	}

	word.Word = req.Word
	word.ListWordRuby = req.ListWordRuby
	word.WordTag = req.WordTag
	word.ListMeaning = req.ListMeaning
	word.ListKanji = listKanji
	word.IsSingleruby = req.IsSingleruby
	word.IsCommon = req.IsCommon
	word.CreateBy = loginUser.UserID
	word.UpdateBy = loginUser.UserID
	err = word.Insert(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	for _, v := range listKanji {
		kanji.Kanji = v
		err = kanji.GetByKanji(ctx, conn)
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				kanji.ListMeaning = make([]string, 0)
				kanji.ListKunReading = make([]string, 0)
				kanji.ListOnReading = make([]string, 0)
				kanji.ListNameReading = make([]string, 0)
				kanji.StrokeCount = 0
				kanji.HeisigEn = ""
				kanji.Unicode = ""
				kanji.JlptLevel = constant.JlptLevelTypeOther
				kanji.SchoolGrade = constant.SchoolGradeTypeOther
				kanji.CreateBy = loginUser.UserID
				kanji.UpdateBy = loginUser.UserID
				err = kanji.Insert(ctx, tx)
				if err != nil {
					errorInternal(c, err)
				}
			} else {
				errorInternal(c, err)
			}
		}
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, word.Res()).SendJSON(c)
}

// @Tags Word
// @Summary update word
// @Description update word
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body wordUpdateReq true "Req Param"
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/update [post]
func (h Word) Update(c echo.Context) error {
	var err error
	var word model.PublicWord
	var kanji model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(wordUpdateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}

	defer db.DeferHandleTransaction(ctx, tx)

	word.WordID = req.WordID
	err = word.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	listKanji := make([]string, 0)
	chars := []rune(req.Word)
	for i := 0; i < len(chars); i++ {
		if validator.RegKanji.MatchString(string(chars[i])) {
			listKanji = append(listKanji, string(chars[i]))
		}
	}

	word.ListMeaning = req.ListMeaning
	word.Word = req.Word
	word.WordTag = req.WordTag
	word.ListWordRuby = req.ListWordRuby
	word.ListKanji = listKanji
	word.IsSingleruby = req.IsSingleruby
	word.IsCommon = req.IsCommon
	word.UpdateBy = loginUser.UserID
	err = word.Update(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	for _, v := range listKanji {
		kanji.Kanji = v
		err = kanji.GetByKanji(ctx, conn)
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				kanji.ListMeaning = make([]string, 0)
				kanji.ListKunReading = make([]string, 0)
				kanji.ListOnReading = make([]string, 0)
				kanji.ListNameReading = make([]string, 0)
				kanji.StrokeCount = 0
				kanji.HeisigEn = ""
				kanji.Unicode = ""
				kanji.JlptLevel = constant.JlptLevelTypeOther
				kanji.SchoolGrade = constant.SchoolGradeTypeOther
				kanji.CreateBy = loginUser.UserID
				kanji.UpdateBy = loginUser.UserID
				err = kanji.Insert(ctx, tx)
				if err != nil {
					errorInternal(c, err)
				}
			} else {
				errorInternal(c, err)
			}
		}
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success("Success update", word.Res()).SendJSON(c)

}

func wordPageData(req *wordPageReq) (error, int, []model.WordRes) {
	var err error
	var cnt int
	var list []model.WordRes
	var count struct{ Count int }

	qSelect := model.GetWordQuery().Where()
	qCount := db.Query(`SELECT COUNT(*) from public.word`).Where()

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	err = pgxscan.Get(ctx, conn, &count, qCount.Build(), qCount.Params()...)
	if err != nil {
		return err, cnt, list
	}

	cnt = count.Count

	if req.GetPage() < 1 {
		req.SetPage(1)
	}

	qSelect.OffsetLimit(req.GetOffset(), req.GetLimit())
	err = pgxscan.Select(ctx, conn, &list, qSelect.Build(), qSelect.Params()...)
	if err != nil {
		return err, cnt, list
	}
	if len(list) == 0 {
		list = make([]model.WordRes, 0)
	}

	return err, cnt, list
}
