package controller

import (
	"errors"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
	"kanji/constant"
	"kanji/db"
	"kanji/model"
	"kanji/request"
	"kanji/response"
	"kanji/validator"
	"strconv"
)

type Sentence struct{}

func SentenceComposer() Sentence {
	return Sentence{}
}

type sentencePageReq struct {
	request.Paging
}

type sentenceCreateReq struct {
	Sentence    string   `db:"sentence,use_zero" json:"sentence" form:"sentence" validate:"required,lte=160"`
	Meaning     string   `db:"meaning,use_zero" json:"meaning" form:"meaning" validate:"required,lte=500"`
	Description string   `db:"description,use_zero" json:"description" form:"description" validate:"lte=500"`
	ListRuby    []string `db:"list_ruby,array,use_zero" json:"listRuby" form:"listRuby" validate:"required"`
	ListRubyTr  []string `db:"list_ruby_tr,array,use_zero" json:"listRubyTr" form:"listRubyTr" validate:"required"`
}

type sentenceUpdateReq struct {
	SentenceID  int64    `db:"sentence_id,pk" json:"sentenceId" form:"sentenceId" validate:"required,existsdata=sentence_id sentenceId"`
	Sentence    string   `db:"sentence,use_zero" json:"sentence" form:"sentence" validate:"required,lte=160"`
	Meaning     string   `db:"meaning,use_zero" json:"meaning" form:"meaning" validate:"required,lte=500"`
	Description string   `db:"description,use_zero" json:"description" form:"description" validate:"lte=500"`
	ListRuby    []string `db:"list_ruby,array,use_zero" json:"listRuby" form:"listRuby" validate:"required"`
	ListRubyTr  []string `db:"list_ruby_tr,array,use_zero" json:"listRubyTr" form:"listRubyTr" validate:"required"`
}

// @Tags Sentence
// @Summary Page Sentence
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query sentencePageReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=response.Pagination{list=[]model.SentenceRes}} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence [post]
func (h Sentence) Page(c echo.Context) error {
	var err error

	req := new(sentencePageReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	err, cnt, list := sentnecePageData(req)
	if err != nil {
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, response.PayloadPagination(req, list, cnt)).SendJSON(c)
}

// @Tags Sentence
// @Summary get sentence by id
// @Accept json
// @Produce json
// @in header
// @Param kanji path number true "sentence_id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.SentenceRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/id/{sentence_id} [get]
func (h Sentence) GetById(c echo.Context) error {
	var err error
	var sentence model.PublicSentence

	ID, err := strconv.ParseInt(c.Param("sentence_id"), 10, 64)
	if err != nil {

		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	sentence.SentenceID = ID
	err = sentence.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, sentence.Res()).SendJSON(c)
}

// @Tags Sentence
// @Summary Create Sentence
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body sentenceCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=model.SentenceRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/create [post]
func (h Sentence) Create(c echo.Context) error {
	var err error
	var sentence model.PublicSentence
	var kanji model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(sentenceCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	listKanji := make([]string, 0)
	chars := []rune(req.Sentence)
	for i := 0; i < len(chars); i++ {
		if validator.RegKanji.MatchString(string(chars[i])) {
			listKanji = append(listKanji, string(chars[i]))
		}
	}

	sentence.Sentence = req.Sentence
	sentence.Meaning = req.Meaning
	sentence.Description = req.Description
	sentence.ListRuby = req.ListRuby
	sentence.ListRubyTr = req.ListRubyTr
	sentence.ListKanji = listKanji
	sentence.CreateBy = loginUser.UserID
	sentence.UpdateBy = loginUser.UserID
	err = sentence.Insert(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	for _, v := range sentence.ListKanji {
		kanji.Kanji = v
		err = kanji.GetByKanji(ctx, conn)
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				kanji.ListMeaning = make([]string, 0)
				kanji.ListKunReading = make([]string, 0)
				kanji.ListOnReading = make([]string, 0)
				kanji.ListNameReading = make([]string, 0)
				kanji.StrokeCount = 0
				kanji.HeisigEn = ""
				kanji.Unicode = ""
				kanji.JlptLevel = constant.JlptLevelTypeOther
				kanji.SchoolGrade = constant.SchoolGradeTypeOther
				kanji.CreateBy = loginUser.UserID
				kanji.UpdateBy = loginUser.UserID
				err = kanji.Insert(ctx, tx)
				if err != nil {
					errorInternal(c, err)
				}
			} else {
				errorInternal(c, err)
			}
		}
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, sentence.Res()).SendJSON(c)
}

// @Tags Sentence
// @Summary Update sentence
// @Description Update sentence
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body sentenceUpdateReq true "Req Param"
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/update [post]
func (h Sentence) Update(c echo.Context) error {
	var err error
	var sentence model.PublicSentence
	var kanji model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(sentenceUpdateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}

	defer db.DeferHandleTransaction(ctx, tx)

	sentence.SentenceID = req.SentenceID
	err = sentence.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	listKanji := make([]string, 0)
	chars := []rune(req.Sentence)
	for i := 0; i < len(chars); i++ {
		if validator.RegKanji.MatchString(string(chars[i])) {
			listKanji = append(listKanji, string(chars[i]))
		}
	}

	sentence.Sentence = req.Sentence
	sentence.Meaning = req.Meaning
	sentence.Description = req.Description
	sentence.ListRuby = req.ListRuby
	sentence.ListRubyTr = req.ListRubyTr
	sentence.ListKanji = listKanji
	sentence.UpdateBy = loginUser.UserID
	err = sentence.Update(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	for _, v := range sentence.ListKanji {
		kanji.Kanji = v
		err = kanji.GetByKanji(ctx, conn)
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				kanji.ListMeaning = make([]string, 0)
				kanji.ListKunReading = make([]string, 0)
				kanji.ListOnReading = make([]string, 0)
				kanji.ListNameReading = make([]string, 0)
				kanji.StrokeCount = 0
				kanji.HeisigEn = ""
				kanji.Unicode = ""
				kanji.JlptLevel = constant.JlptLevelTypeOther
				kanji.SchoolGrade = constant.SchoolGradeTypeOther
				kanji.CreateBy = loginUser.UserID
				kanji.UpdateBy = loginUser.UserID
				err = kanji.Insert(ctx, tx)
				if err != nil {
					errorInternal(c, err)
				}
			} else {
				errorInternal(c, err)
			}
		}
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success("Success update", sentence.Res()).SendJSON(c)

}

func sentnecePageData(req *sentencePageReq) (error, int, []model.SentenceRes) {
	var err error
	var cnt int
	var list []model.SentenceRes
	var count struct{ Count int }

	qSelect := model.GetSentenceQuery().Where()
	qCount := db.Query(`SELECT COUNT(*) from public.sentence`).Where()

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	err = pgxscan.Get(ctx, conn, &count, qCount.Build(), qCount.Params()...)
	if err != nil {
		return err, cnt, list
	}

	cnt = count.Count

	if req.GetPage() < 1 {
		req.SetPage(1)
	}

	qSelect.OffsetLimit(req.GetOffset(), req.GetLimit())
	err = pgxscan.Select(ctx, conn, &list, qSelect.Build(), qSelect.Params()...)
	if err != nil {
		return err, cnt, list
	}
	if len(list) == 0 {
		list = make([]model.SentenceRes, 0)
	}

	return err, cnt, list
}
