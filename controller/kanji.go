package controller

import (
	"context"
	"errors"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	"kanji/db"
	"kanji/model"
	"kanji/request"
	"kanji/response"
	"net/url"
	"strconv"
)

type Kanji struct{}

func KanjiComposer() Kanji {
	return Kanji{}
}

type kanjiPageReq struct {
	request.Paging
}

type kanjiCreateReq struct {
	Kanji           string   `db:"kanji,use_zero" json:"kanji" form:"kanji" validate:"required,notexists=kanji kanji"`
	ListMeaning     []string `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" validate:"required"`
	ListKunReading  []string `db:"list_kun_reading,array,use_zero" json:"listKunReading" form:"listKunReading" validate:"required"`
	ListOnReading   []string `db:"list_on_reading,array,use_zero" json:"listOnReading" form:"listOnReading" validate:"required"`
	ListNameReading []string `db:"list_name_reading,array,use_zero" json:"listNameReading" form:"listNameReading" validate:"required"`
	StrokeCount     int      `db:"stroke_count,use_zero" json:"strokeCount" form:"strokeCount" validate:"required"`
	HeisigEn        string   `db:"heisig_en,use_zero" json:"heisigEn" form:"heisigEn" validate:"required,lte=80"`
	Unicode         string   `db:"unicode,use_zero" json:"unicode" form:"unicode" validate:"lte=10"`
	JlptLevel       string   `db:"jlpt_level,use_zero" json:"jlptLevel" form:"jlptLevel" validate:"required,oneof='JLPT_N4' 'JLPT_N3' 'JLPT_N2' 'JLPT_N1' 'OTHER' 'JLPT_N5'"`
	SchoolGrade     string   `db:"school_grade,use_zero" json:"schoolGrade" form:"schoolGrade" validate:"required,oneof='ELEMENTARY_6' 'SECONDARY_1' 'SECONDARY_2' 'SECONDARY_3' 'SECONDARY_4' 'SECONDARY_5' 'SECONDARY_6' 'OTHER' 'ADVENCED' 'ELEMENTARY_1' 'ELEMENTARY_2' 'ELEMENTARY_3' 'ELEMENTARY_4' 'ELEMENTARY_5'"`
}

type kanjiUpdateReq struct {
	KanjiID         int64    `json:"kanjiId" form:"kanjiId" query:"kanjiId" validate:"required,existsdata=kanji_id kanjiId"`
	Kanji           string   `db:"kanji,use_zero" json:"kanji" form:"kanji" validate:"required"`
	ListMeaning     []string `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" validate:"required"`
	ListKunReading  []string `db:"list_kun_reading,array,use_zero" json:"listKunReading" form:"listKunReading" validate:"required"`
	ListOnReading   []string `db:"list_on_reading,array,use_zero" json:"listOnReading" form:"listOnReading" validate:"required"`
	ListNameReading []string `db:"list_name_reading,array,use_zero" json:"listNameReading" form:"listNameReading" validate:"required"`
	StrokeCount     int      `db:"stroke_count,use_zero" json:"strokeCount" form:"strokeCount" validate:"required"`
	HeisigEn        string   `db:"heisig_en,use_zero" json:"heisigEn" form:"heisigEn" validate:"required,lte=80"`
	Unicode         string   `db:"unicode,use_zero" json:"unicode" form:"unicode" validate:"lte=10"`
	JlptLevel       string   `db:"jlpt_level,use_zero" json:"jlptLevel" form:"jlptLevel" validate:"required,oneof='JLPT_N4' 'JLPT_N3' 'JLPT_N2' 'JLPT_N1' 'OTHER' 'JLPT_N5'"`
	SchoolGrade     string   `db:"school_grade,use_zero" json:"schoolGrade" form:"schoolGrade" validate:"required,oneof='ELEMENTARY_6' 'SECONDARY_1' 'SECONDARY_2' 'SECONDARY_3' 'SECONDARY_4' 'SECONDARY_5' 'SECONDARY_6' 'OTHER' 'ADVENCED' 'ELEMENTARY_1' 'ELEMENTARY_2' 'ELEMENTARY_3' 'ELEMENTARY_4' 'ELEMENTARY_5'"`
}

// @Tags Kanji
// @Summary Page Kanji
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query kanjiPageReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=response.Pagination{list=[]model.KanjiRes}} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji [post]
func (h Kanji) Page(c echo.Context) error {
	var err error

	req := new(kanjiPageReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	err, cnt, list := kanjiPageData(req)
	if err != nil {
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, response.PayloadPagination(req, list, cnt)).SendJSON(c)
}

// @Tags Kanji
// @Summary get kanji by id
// @Accept json
// @Produce json
// @in header
// @Param kanji path number true "kanji_id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/id/{kanji_id} [get]
func (h Kanji) GetById(c echo.Context) error {
	var err error
	var kanji model.PublicKanji

	ID, err := strconv.ParseInt(c.Param("kanji_id"), 10, 64)
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.KanjiID = ID
	err = kanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, kanji.Res()).SendJSON(c)
}

// @Tags Kanji
// @Summary get kanji by kanji
// @Accept json
// @Produce json
// @in header
// @Param kanji path string true "kanji" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/kanji/{kanji} [get]
func (h Kanji) GetByKanji(c echo.Context) error {
	var err error
	var kanji model.PublicKanji

	reqKanji, err := url.QueryUnescape(c.Param("kanji"))
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.Kanji = reqKanji
	err = kanji.GetByKanji(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, kanji.Res()).SendJSON(c)
}

// @Tags Kanji
// @Summary get kanji by id detail
// @Accept json
// @Produce json
// @in header
// @Param kanji path number true "kanji_id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiResDetail} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/id/detail/{kanji_id} [get]
func (h Kanji) GetByIdDetail(c echo.Context) error {
	var err error
	var kanji model.PublicKanji

	ID, err := strconv.ParseInt(c.Param("kanji_id"), 10, 64)
	if err != nil {

		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.KanjiID = ID
	err = kanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return h.getKanjiDetail(c, conn, ctx, kanji)
}

// @Tags Kanji
// @Summary get kanji by kanji
// @Accept json
// @Produce json
// @in header
// @Param kanji path string true "kanji" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/kanji/detail/{kanji} [get]
func (h Kanji) GetByKanjiDetail(c echo.Context) error {
	var err error
	var kanji model.PublicKanji

	reqKanji, err := url.QueryUnescape(c.Param("kanji"))
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.Kanji = reqKanji
	err = kanji.GetByKanji(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return h.getKanjiDetail(c, conn, ctx, kanji)
}

func (h Kanji) getKanjiDetail(c echo.Context, conn *pgxpool.Conn, ctx context.Context, kanji model.PublicKanji) error {
	var listWord []model.WordRes
	var listSentence []model.SentenceRes
	var err error

	qSelectWord := model.GetWordQuery().Where().AnyStringEq(kanji.Kanji, "list_kanji")
	err = pgxscan.Select(ctx, conn, &listWord, qSelectWord.Build(), qSelectWord.Params()...)
	if err != nil {
		errorInternal(c, err)
	}
	if len(listWord) == 0 {
		listWord = make([]model.WordRes, 0)
	}

	qSelectSentence := model.GetSentenceQuery().Where().StringLike("sentence", kanji.Kanji)
	err = pgxscan.Select(ctx, conn, &listSentence, qSelectSentence.Build(), qSelectSentence.Params()...)
	if err != nil {
		errorInternal(c, err)
	}
	if len(listSentence) == 0 {
		listSentence = make([]model.SentenceRes, 0)
	}

	res := model.KanjiResDetail{
		KanjiID:         kanji.KanjiID,
		Kanji:           kanji.Kanji,
		ListMeaning:     kanji.ListMeaning,
		ListKunReading:  kanji.ListKunReading,
		ListOnReading:   kanji.ListOnReading,
		ListNameReading: kanji.ListNameReading,
		StrokeCount:     kanji.StrokeCount,
		HeisigEn:        kanji.HeisigEn,
		Unicode:         kanji.Unicode,
		JlptLevel:       kanji.JlptLevel,
		SchoolGrade:     kanji.SchoolGrade,
		CreateBy:        kanji.CreateBy,
		CreateDt:        kanji.CreateDt,
		UpdateBy:        kanji.UpdateBy,
		UpdateDt:        kanji.UpdateDt,
		ListWord:        listWord,
		ListSentence:    listSentence,
	}

	return response.Success(response.ResponseSuccess, res).SendJSON(c)
}

// @Tags Kanji
// @Summary Create Kanji
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body kanjiCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/create [post]
func (h Kanji) Create(c echo.Context) error {
	var err error
	var kanji model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(kanjiCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	kanji.Kanji = req.Kanji
	kanji.ListMeaning = req.ListMeaning
	kanji.ListKunReading = req.ListKunReading
	kanji.ListOnReading = req.ListOnReading
	kanji.ListNameReading = req.ListNameReading
	kanji.StrokeCount = req.StrokeCount
	kanji.HeisigEn = req.HeisigEn
	kanji.Unicode = req.Unicode
	kanji.JlptLevel = req.JlptLevel
	kanji.SchoolGrade = req.SchoolGrade
	kanji.CreateBy = loginUser.UserID
	kanji.UpdateBy = loginUser.UserID
	err = kanji.Insert(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, kanji.Res()).SendJSON(c)
}

// @Tags Kanji
// @Summary update kanji
// @Description update kanji
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body kanjiUpdateReq true "Req Param"
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/update [post]
func (h Kanji) Update(c echo.Context) error {
	var err error
	var kanji model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(kanjiUpdateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}

	defer db.DeferHandleTransaction(ctx, tx)

	kanji.KanjiID = req.KanjiID
	err = kanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	kanji.ListMeaning = req.ListMeaning
	kanji.ListKunReading = req.ListKunReading
	kanji.ListOnReading = req.ListOnReading
	kanji.ListNameReading = req.ListNameReading
	kanji.StrokeCount = req.StrokeCount
	kanji.HeisigEn = req.HeisigEn
	kanji.Unicode = req.Unicode
	kanji.JlptLevel = req.JlptLevel
	kanji.SchoolGrade = req.SchoolGrade
	kanji.UpdateBy = loginUser.UserID
	err = kanji.Update(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success("Success update", kanji.Res()).SendJSON(c)
}

func kanjiPageData(req *kanjiPageReq) (error, int, []model.KanjiRes) {
	var err error
	var cnt int
	var list []model.KanjiRes
	var count struct{ Count int }

	qSelect := model.GetKanjiQuery().Where()
	qCount := db.Query(`SELECT COUNT(*) from public.kanji`).Where()

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	err = pgxscan.Get(ctx, conn, &count, qCount.Build(), qCount.Params()...)
	if err != nil {
		return err, cnt, list
	}

	cnt = count.Count

	if req.GetPage() < 1 {
		req.SetPage(1)
	}

	qSelect.OffsetLimit(req.GetOffset(), req.GetLimit())
	err = pgxscan.Select(ctx, conn, &list, qSelect.Build(), qSelect.Params()...)
	if err != nil {
		return err, cnt, list
	}
	if len(list) == 0 {
		list = make([]model.KanjiRes, 0)
	}

	return err, cnt, list
}
