package controller

import (
	"encoding/binary"
	"errors"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"github.com/labstack/echo/v4"
	"kanji/constant"
	"kanji/cryption"
	"kanji/log"
	"kanji/response"
	"kanji/validator"
	"sync/atomic"
	"time"
)

var json jsoniter.API
var Validate *validator.CustomValidator
var ErrNilLastLogin error
var CacheUserAuth map[int64]atomic.Value

type UserAuthToken struct {
	PassVersion int
	LastLogin   int64
}

func init() {
	json = jsoniter.ConfigFastest
	Validate = validator.NewValidator()
	ErrNilLastLogin = errors.New("Last login can not be nil")
	CacheUserAuth = make(map[int64]atomic.Value)
}

func getUserLoginInfo(c echo.Context) (UserLogin, error) {
	if u, ok := c.Get(constant.TokenUserContext).(UserLogin); ok {
		fmt.Printf("u ", u)
		return u, nil
	} else {
		fmt.Printf("err ")
		return UserLogin{}, response.ErrorForce("Akses tidak diterima", response.Payload{})
	}
}

func Ping(c echo.Context) error {
	return response.Success("Hallo　世界", response.Payload{}).SendJSON(c)
}

type UserLogin struct {
	UserID   int64  `json:"userID"`
	Username string `json:"username"`
	Email    string `json:"email"`
	UserType string `json:"userType"`
	Fullname string `json:"fullname"`
}

func getLoginToken(userID int64, username, email, usertype string, lastLoginDate int64, expiredAt time.Time) ([]byte, error) {
	username = fmt.Sprintf("%-20.20s", username)
	email = fmt.Sprintf("%-20.20s", email)
	usertype = fmt.Sprintf("%-20.20s", usertype)
	expiredUnix := expiredAt.Unix()

	tokenPayload := make([]byte, constant.TokenPayloadLen)
	binary.BigEndian.PutUint64(tokenPayload, uint64(expiredUnix)) // Expired date
	binary.BigEndian.PutUint64(tokenPayload[8:16], uint64(userID))
	copy(tokenPayload[16:36], username)
	copy(tokenPayload[36:56], email)
	copy(tokenPayload[56:76], usertype)

	return cryption.EncryptAES64(tokenPayload)
}

func errorInternal(c echo.Context, err error) {
	log.System.Error().Err(err).Str("Host", c.Request().Host).Str("Path", c.Path()).Send()
	panic(err)
}
