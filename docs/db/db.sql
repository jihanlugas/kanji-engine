CREATE sequence next_id;

CREATE OR REPLACE FUNCTION public.next_id()
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
DECLARE
    seq_id bigint;
BEGIN
SELECT nextval('public.next_id') 
INTO seq_id;
return seq_id;
END;
$function$
;


CREATE TYPE user_type AS ENUM ('ADMIN');

CREATE TYPE jlpt_level_type AS ENUM (
    'JLPT_N5',
    'JLPT_N4',
    'JLPT_N3',
    'JLPT_N2',
    'JLPT_N1',
    'OTHER'
);

CREATE TYPE school_grade_type AS ENUM (
    'ELEMENTARY_1',
    'ELEMENTARY_2',
    'ELEMENTARY_3',
    'ELEMENTARY_4',
    'ELEMENTARY_5',
    'ELEMENTARY_6',
    'SECONDARY_1',
    'SECONDARY_2',
    'SECONDARY_3',
    'SECONDARY_4',
    'SECONDARY_5',
    'SECONDARY_6',
    'ADVENCED',
    'OTHER'
);

CREATE TYPE word_tag_type AS ENUM (
    'JLPT_N5_VOCAB',
    'JLPT_N4_VOCAB',
    'JLPT_N3_VOCAB',
    'JLPT_N2_VOCAB',
    'JLPT_N1_VOCAB',
    'OTHER'
);

CREATE TABLE public.user (
    user_id int8 NOT NULL DEFAULT next_id(),
    fullname varchar(80) NOT NULL,
    no_hp varchar(20) NOT NULL,
    email varchar(200) NOT NULL,
    user_type user_type NOT NULL,
    username varchar(20) NOT NULL,
    passwd varchar(200) NOT NULL,
    is_active bool NOT NULL,
    last_login_dt timestamptz(0) NOT NULL,
    pass_version int4 NOT NULL,
    create_by int8 NOT NULL,
    create_dt timestamptz(0) NOT NULL,
    update_by int8 NOT NULL,
    update_dt timestamptz(0) NOT NULL,
    delete_by int8 NULL,
    delete_dt timestamptz(0) NULL,
    CONSTRAINT user_pk PRIMARY KEY (user_id)
);

CREATE TABLE public.kanji (
    kanji_id int8 NOT NULL DEFAULT next_id(),
    kanji varchar(10) NOT NULL,
    list_meaning varchar(160)[] NOT NULL,
    list_kun_reading varchar(20)[] NOT NULL,
    list_on_reading varchar(20)[] NOT NULL,
    list_name_reading varchar(20)[] NOT NULL,
    stroke_count int NOT NULL,
    heisig_en varchar(80) NOT NULL,
    unicode varchar(10) NOT NULL,
    jlpt_level jlpt_level_type NOT NULL,
    school_grade school_grade_type NOT NULL,
    create_by int8 NOT NULL,
    create_dt timestamptz(0) NOT NULL,
    update_by int8 NOT NULL,
    update_dt timestamptz(0) NOT NULL,
    CONSTRAINT kanji_pk PRIMARY KEY (kanji_id)
);

CREATE TABLE public.word (
    word_id int8 NOT NULL DEFAULT next_id(),
    word varchar(10) NOT NULL,
    word_tag word_tag_type NOT NULL,
    list_word_ruby varchar(10)[] NOT NULL,
    list_meaning varchar(160)[] NOT NULL,
    list_kanji varchar(10)[] NOT NULL,
    is_singleruby boolean NOT NULL,
    is_common boolean NOT NULL,
    create_by int8 NOT NULL,
    create_dt timestamptz(0) NOT NULL,
    update_by int8 NOT NULL,
    update_dt timestamptz(0) NOT NULL,
    CONSTRAINT word_pk PRIMARY KEY (word_id)
);

CREATE TABLE public.sentence (
    sentence_id int8 NOT NULL DEFAULT next_id(),
    sentence varchar(160) NOT NULL,
    meaning varchar(500) NOT NULL,
    description varchar(500) NOT NULL,
    list_ruby varchar(10)[] NOT NULL,
    list_ruby_tr varchar(10)[] NOT NULL,
    list_kanji varchar(10)[] NOT NULL,
    create_by int8 NOT NULL,
    create_dt timestamptz(0) NOT NULL,
    update_by int8 NOT NULL,
    update_dt timestamptz(0) NOT NULL,
    CONSTRAINT sentence_pk PRIMARY KEY (sentence_id)
);


CREATE TABLE public.wordtest (
    wordtest_id int8 NOT NULL DEFAULT next_id(),
    wordtest varchar(10) NOT NULL,
    word_tag word_tag_type[] NOT NULL,
    create_by int8 NOT NULL,
    create_dt timestamptz(0) NOT NULL,
    update_by int8 NOT NULL,
    update_dt timestamptz(0) NOT NULL,
    CONSTRAINT wordtest_pk PRIMARY KEY (wordtest_id)
);






