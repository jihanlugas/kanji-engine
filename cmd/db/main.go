package main

import (
	"fmt"
	"io/ioutil"
	"kanji/db"
	"net/http"
	"os"
)

func main() {
	var err error

	dbpool := db.Initialize()
	defer dbpool.Close()

	sqlContent, err := ioutil.ReadFile("./docs/db/db.sql")
	if err != nil {
		panic(err)
	}
	sql := string(sqlContent)

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		panic(err)
		os.Exit(http.StatusInternalServerError)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	exec, err := tx.Exec(ctx, sql)
	if err != nil {
		fmt.Println("err ", err)
		panic(err)
		os.Exit(http.StatusInternalServerError)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		panic(err)
		os.Exit(http.StatusInternalServerError)
	}

	fmt.Println("exec ", exec)
	fmt.Println("success")
}
