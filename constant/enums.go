//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package constant

const (
	JlptLevelTypeJlptN1        = "JLPT_N1"
	JlptLevelTypeJlptN2        = "JLPT_N2"
	JlptLevelTypeJlptN3        = "JLPT_N3"
	JlptLevelTypeJlptN4        = "JLPT_N4"
	JlptLevelTypeJlptN5        = "JLPT_N5"
	JlptLevelTypeOther         = "OTHER"
	SchoolGradeTypeSecondary3  = "SECONDARY_3"
	SchoolGradeTypeSecondary2  = "SECONDARY_2"
	SchoolGradeTypeSecondary1  = "SECONDARY_1"
	SchoolGradeTypeElementary6 = "ELEMENTARY_6"
	SchoolGradeTypeElementary5 = "ELEMENTARY_5"
	SchoolGradeTypeElementary4 = "ELEMENTARY_4"
	SchoolGradeTypeElementary3 = "ELEMENTARY_3"
	SchoolGradeTypeElementary1 = "ELEMENTARY_1"
	SchoolGradeTypeElementary2 = "ELEMENTARY_2"
	SchoolGradeTypeOther       = "OTHER"
	SchoolGradeTypeAdvenced    = "ADVENCED"
	SchoolGradeTypeSecondary6  = "SECONDARY_6"
	SchoolGradeTypeSecondary5  = "SECONDARY_5"
	SchoolGradeTypeSecondary4  = "SECONDARY_4"
	UserTypeAdmin              = "ADMIN"
	WordTagTypeOther           = "OTHER"
	WordTagTypeJlptN1Vocab     = "JLPT_N1_VOCAB"
	WordTagTypeJlptN2Vocab     = "JLPT_N2_VOCAB"
	WordTagTypeJlptN3Vocab     = "JLPT_N3_VOCAB"
	WordTagTypeJlptN4Vocab     = "JLPT_N4_VOCAB"
	WordTagTypeJlptN5Vocab     = "JLPT_N5_VOCAB"
)
