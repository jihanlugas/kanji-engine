package model

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"kanji/db"
	"kanji/validator"
	"time"
)

type WordRes struct {
	WordID       int64      `json:"wordId"`
	Word         string     `json:"word"`
	WordTag      string     `json:"wordTag"`
	ListMeaning  []string   `json:"listMeaning"`
	ListWordRuby []string   `json:"listWordRuby"`
	ListKanji    []string   `json:"listKanji"`
	IsSingleruby bool       `json:"isSingleruby"`
	IsCommon     bool       `json:"isCommon"`
	CreateBy     int64      `json:"-"`
	CreateDt     *time.Time `json:"-"`
	UpdateBy     int64      `json:"-"`
	UpdateDt     *time.Time `json:"-"`
}

type WordResDetail struct {
	WordID       int64      `json:"wordId"`
	Word         string     `json:"word"`
	WordTag      string     `json:"wordTag"`
	ListMeaning  []string   `json:"listMeaning"`
	ListWordRuby []string   `json:"listWordRuby"`
	ListKanji    []string   `json:"listKanji"`
	IsSingleruby bool       `json:"isSingleruby"`
	IsCommon     bool       `json:"isCommon"`
	CreateBy     int64      `json:"-"`
	CreateDt     *time.Time `json:"-"`
	UpdateBy     int64      `json:"-"`
	UpdateDt     *time.Time `json:"-"`
}

func GetWordQuery() *db.QueryComposer {
	return db.Query(`SELECT word_id, word, word_tag, list_meaning, list_word_ruby, list_kanji, is_singleruby, is_common, create_by, create_dt, update_by, update_dt FROM public.word`)
}

func (p *PublicWord) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetWordQuery().
		Where().
		Int64("word_id", "=", p.WordID).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicWord) GetByWord(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetWordQuery().
		Where().
		StringEq("word", p.Word).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicWord) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.word
		(word, word_tag, list_word_ruby, list_meaning, list_kanji, is_singleruby, is_common, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
		RETURNING word_id;`,
		p.Word,
		p.WordTag,
		p.ListWordRuby,
		p.ListMeaning,
		p.ListKanji,
		p.IsSingleruby,
		p.IsCommon,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.WordID)

	return err
}

func (p *PublicWord) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	p.ListKanji = make([]string, 0)
	chars := []rune(p.Word)
	for i := 0; i < len(chars); i++ {
		if validator.RegKanji.MatchString(string(chars[i])) {
			p.ListKanji = append(p.ListKanji, string(chars[i]))
		}
	}

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.word SET word = $1
		, word_tag = $2
		, list_word_ruby = $3
		, list_meaning = $4
		, list_kanji = $5
		, is_singleruby = $6
		, is_common = $7
		, update_by = $8
		, update_dt = $9
		WHERE word_id = $10`,
		p.Word,
		p.WordTag,
		p.ListWordRuby,
		p.ListMeaning,
		p.ListKanji,
		p.IsSingleruby,
		p.IsCommon,
		p.UpdateBy,
		p.UpdateDt,
		p.WordID,
	)

	return err
}

func (p *PublicWord) Res() WordRes {
	res := WordRes{
		WordID:       p.WordID,
		Word:         p.Word,
		WordTag:      p.WordTag,
		ListWordRuby: p.ListWordRuby,
		ListMeaning:  p.ListMeaning,
		ListKanji:    p.ListKanji,
		IsSingleruby: p.IsSingleruby,
		IsCommon:     p.IsCommon,
	}

	return res
}

func (p *PublicWord) ResDetail() WordResDetail {
	res := WordResDetail{
		WordID:       p.WordID,
		Word:         p.Word,
		WordTag:      p.WordTag,
		ListWordRuby: p.ListWordRuby,
		ListMeaning:  p.ListMeaning,
		ListKanji:    p.ListKanji,
		IsSingleruby: p.IsSingleruby,
		IsCommon:     p.IsCommon,
	}

	return res
}
