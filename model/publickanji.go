//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicKanji struct {
	KanjiID         int64      `db:"kanji_id,pk" json:"kanjiId" form:"kanjiId" validate:"required"`
	Kanji           string     `db:"kanji,use_zero" json:"kanji" form:"kanji" validate:"required,lte=10"`
	ListMeaning     []string   `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" validate:"required"`
	ListKunReading  []string   `db:"list_kun_reading,array,use_zero" json:"listKunReading" form:"listKunReading" validate:"required"`
	ListOnReading   []string   `db:"list_on_reading,array,use_zero" json:"listOnReading" form:"listOnReading" validate:"required"`
	ListNameReading []string   `db:"list_name_reading,array,use_zero" json:"listNameReading" form:"listNameReading" validate:"required"`
	StrokeCount     int        `db:"stroke_count,use_zero" json:"strokeCount" form:"strokeCount" validate:"required"`
	HeisigEn        string     `db:"heisig_en,use_zero" json:"heisigEn" form:"heisigEn" validate:"required,lte=80"`
	Unicode         string     `db:"unicode,use_zero" json:"unicode" form:"unicode" validate:"required,lte=10"`
	JlptLevel       string     `db:"jlpt_level,use_zero" json:"jlptLevel" form:"jlptLevel" validate:"required,oneof='JLPT_N1' 'JLPT_N2' 'JLPT_N3' 'JLPT_N4' 'JLPT_N5' 'OTHER'"`
	SchoolGrade     string     `db:"school_grade,use_zero" json:"schoolGrade" form:"schoolGrade" validate:"required,oneof='SECONDARY_3' 'SECONDARY_2' 'SECONDARY_1' 'ELEMENTARY_6' 'ELEMENTARY_5' 'ELEMENTARY_4' 'ELEMENTARY_3' 'ELEMENTARY_1' 'ELEMENTARY_2' 'OTHER' 'ADVENCED' 'SECONDARY_6' 'SECONDARY_5' 'SECONDARY_4'"`
	CreateBy        int64      `db:"create_by,use_zero" json:"createBy" form:"createBy" validate:"required"`
	CreateDt        *time.Time `db:"create_dt,use_zero" json:"createDt" form:"createDt" validate:"required"`
	UpdateBy        int64      `db:"update_by,use_zero" json:"updateBy" form:"updateBy" validate:"required"`
	UpdateDt        *time.Time `db:"update_dt,use_zero" json:"updateDt" form:"updateDt" validate:"required"`
}
