//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"fmt"
	"time"
)

type CustomEnum []string

type PublicWordtest struct {
	WordtestID int64      `db:"wordtest_id,pk" json:"wordtestId" form:"wordtestId" validate:"required"`
	Wordtest   string     `db:"wordtest,use_zero" json:"wordtest" form:"wordtest" validate:"required,lte=10"`
	WordTag    []string   `db:"word_tag,array,use_zero" json:"wordTag" form:"wordTag" validate:"required,dive,oneof='JLPT_N5_VOCAB' 'JLPT_N4_VOCAB' 'JLPT_N3_VOCAB' 'JLPT_N2_VOCAB' 'JLPT_N1_VOCAB' 'OTHER'"`
	CreateBy   int64      `db:"create_by,use_zero" json:"createBy" form:"createBy" validate:"required"`
	CreateDt   *time.Time `db:"create_dt,use_zero" json:"createDt" form:"createDt" validate:"required"`
	UpdateBy   int64      `db:"update_by,use_zero" json:"updateBy" form:"updateBy" validate:"required"`
	UpdateDt   *time.Time `db:"update_dt,use_zero" json:"updateDt" form:"updateDt" validate:"required"`
}

func (ce *CustomEnum) Scan(b []byte) error {
	var err error

	fmt.Println(b)

	return err
}
