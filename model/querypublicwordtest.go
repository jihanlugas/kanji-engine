package model

import (
	"kanji/db"
)

func GetWordtestQuery() *db.QueryComposer {
	return db.Query(`SELECT wordtest_id, wordtest, word_tag, create_by, create_dt, update_by, update_dt FROM public.wordtest`)
}
