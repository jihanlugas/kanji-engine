package model

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"kanji/db"
	"time"
)

type KanjiRes struct {
	KanjiID         int64      `json:"kanjiId"`
	Kanji           string     `json:"kanji"`
	ListMeaning     []string   `json:"listMeaning"`
	ListKunReading  []string   `json:"listKunReading"`
	ListOnReading   []string   `json:"listOnReading"`
	ListNameReading []string   `json:"listNameReading"`
	StrokeCount     int        `json:"strokeCount"`
	HeisigEn        string     `json:"heisigEn"`
	Unicode         string     `json:"unicode"`
	JlptLevel       string     `json:"jlptLevel"`
	SchoolGrade     string     `json:"schoolGrade"`
	CreateBy        int64      `json:"-"`
	CreateDt        *time.Time `json:"-"`
	UpdateBy        int64      `json:"-"`
	UpdateDt        *time.Time `json:"-"`
}

type KanjiResDetail struct {
	KanjiID         int64         `json:"kanjiId"`
	Kanji           string        `json:"kanji"`
	ListMeaning     []string      `json:"listMeaning"`
	ListKunReading  []string      `json:"listKunReading"`
	ListOnReading   []string      `json:"listOnReading"`
	ListNameReading []string      `json:"listNameReading"`
	StrokeCount     int           `json:"strokeCount"`
	HeisigEn        string        `json:"heisigEn"`
	Unicode         string        `json:"unicode"`
	JlptLevel       string        `json:"jlptLevel"`
	SchoolGrade     string        `json:"schoolGrade"`
	CreateBy        int64         `json:"-"`
	CreateDt        *time.Time    `json:"-"`
	UpdateBy        int64         `json:"-"`
	UpdateDt        *time.Time    `json:"-"`
	ListWord        []WordRes     `json:"listWord"`
	ListSentence    []SentenceRes `json:"listSentence"`
}

func GetKanjiQuery() *db.QueryComposer {
	return db.Query(`SELECT kanji_id, kanji, list_meaning, list_kun_reading, list_on_reading, list_name_reading, stroke_count, heisig_en, unicode, jlpt_level, school_grade, create_by, create_dt, update_by, update_dt FROM public.kanji`)
}

func (p *PublicKanji) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetKanjiQuery().
		Where().
		Int64("kanji_id", "=", p.KanjiID).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicKanji) GetByKanji(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetKanjiQuery().
		Where().
		StringEq("kanji", p.Kanji).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicKanji) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.kanji
		(kanji, list_meaning, list_kun_reading, list_on_reading, list_name_reading, stroke_count, heisig_en, unicode, jlpt_level, school_grade, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
		RETURNING kanji_id;`,
		p.Kanji,
		p.ListMeaning,
		p.ListKunReading,
		p.ListOnReading,
		p.ListNameReading,
		p.StrokeCount,
		p.HeisigEn,
		p.Unicode,
		p.JlptLevel,
		p.SchoolGrade,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.KanjiID)

	return err
}

func (p *PublicKanji) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.kanji SET list_meaning = $1
		, list_kun_reading = $2
		, list_on_reading = $3
		, list_name_reading = $4
		, stroke_count = $5
		, heisig_en = $6
		, unicode = $7
		, jlpt_level = $8
		, school_grade = $9
		, update_by = $10
		, update_dt= $11
		WHERE kanji_id = $12`,
		p.ListMeaning,
		p.ListKunReading,
		p.ListOnReading,
		p.ListNameReading,
		p.StrokeCount,
		p.HeisigEn,
		p.Unicode,
		p.JlptLevel,
		p.SchoolGrade,
		p.UpdateBy,
		p.UpdateDt,
		p.KanjiID,
	)

	return err
}

func (p *PublicKanji) Res() KanjiRes {
	res := KanjiRes{
		KanjiID:         p.KanjiID,
		Kanji:           p.Kanji,
		ListMeaning:     p.ListMeaning,
		ListKunReading:  p.ListKunReading,
		ListOnReading:   p.ListOnReading,
		ListNameReading: p.ListNameReading,
		StrokeCount:     p.StrokeCount,
		HeisigEn:        p.HeisigEn,
		Unicode:         p.Unicode,
		JlptLevel:       p.JlptLevel,
		SchoolGrade:     p.SchoolGrade,
	}

	return res
}

func (p *PublicKanji) ResDetail() KanjiResDetail {
	res := KanjiResDetail{
		KanjiID:         p.KanjiID,
		Kanji:           p.Kanji,
		ListMeaning:     p.ListMeaning,
		ListKunReading:  p.ListKunReading,
		ListOnReading:   p.ListOnReading,
		ListNameReading: p.ListNameReading,
		StrokeCount:     p.StrokeCount,
		HeisigEn:        p.HeisigEn,
		Unicode:         p.Unicode,
		JlptLevel:       p.JlptLevel,
		SchoolGrade:     p.SchoolGrade,
	}

	return res

}
