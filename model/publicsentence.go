//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicSentence struct {
	SentenceID  int64      `db:"sentence_id,pk" json:"sentenceId" form:"sentenceId" validate:"required"`
	Sentence    string     `db:"sentence,use_zero" json:"sentence" form:"sentence" validate:"required,lte=160"`
	Meaning     string     `db:"meaning,use_zero" json:"meaning" form:"meaning" validate:"required,lte=500"`
	Description string     `db:"description,use_zero" json:"description" form:"description" validate:"required,lte=500"`
	ListRuby    []string   `db:"list_ruby,array,use_zero" json:"listRuby" form:"listRuby" validate:"required"`
	ListRubyTr  []string   `db:"list_ruby_tr,array,use_zero" json:"listRubyTr" form:"listRubyTr" validate:"required"`
	ListKanji   []string   `db:"list_kanji,array,use_zero" json:"listKanji" form:"listKanji" validate:"required"`
	CreateBy    int64      `db:"create_by,use_zero" json:"createBy" form:"createBy" validate:"required"`
	CreateDt    *time.Time `db:"create_dt,use_zero" json:"createDt" form:"createDt" validate:"required"`
	UpdateBy    int64      `db:"update_by,use_zero" json:"updateBy" form:"updateBy" validate:"required"`
	UpdateDt    *time.Time `db:"update_dt,use_zero" json:"updateDt" form:"updateDt" validate:"required"`
}
