package model

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"kanji/db"
	"strings"
	"time"
)

func GetUserQuery() *db.QueryComposer {
	return db.Query(`SELECT user_id, fullname, email, user_type, username, no_hp, passwd, is_active, last_login_dt, pass_version, create_by, create_dt, update_by, update_dt FROM public.user`)
}

func (p *PublicUser) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetUserQuery().
		Where().
		Int64(`user_id`, "=", int64(p.UserID)).
		IsNull(`delete_dt`).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicUser) GetByUsername(ctx context.Context, conn *pgxpool.Conn) error {
	var err error
	p.Username = strings.ToLower(p.Username)

	sql := GetUserQuery().
		Where().
		StringEq(`username`, p.Username).
		IsNull(`delete_dt`).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicUser) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.Username = strings.ToLower(p.Username)
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.user (fullname, email, user_type, username, no_hp, passwd, is_active, last_login_dt, pass_version, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
		RETURNING user_id;`,
		p.Fullname,
		p.Email,
		p.UserType,
		p.Username,
		p.NoHp,
		p.Passwd,
		p.IsActive,
		p.LastLoginDt,
		p.PassVersion,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.UserID)
	return err
}

// passwd not updated
func (p *PublicUser) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.Username = strings.ToLower(p.Username)
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.user SET fullname = $1
		, email = $2
		, user_type = $3
		, username = $4
		, no_hp = $5
		, is_active = $6
		, last_login_dt = $7
		, pass_version = $8
		, update_by = $9
		, update_dt = $10
		WHERE user_id = $11`,
		p.Fullname,
		p.Email,
		p.UserType,
		p.Username,
		p.NoHp,
		p.IsActive,
		p.LastLoginDt,
		p.PassVersion,
		p.UpdateBy,
		p.UpdateDt,
		p.UserID,
	)
	return err
}
