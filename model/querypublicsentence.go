package model

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"kanji/db"
	"time"
)

type SentenceRes struct {
	SentenceID  int64      `json:"sentenceId"`
	Sentence    string     `json:"sentence"`
	Meaning     string     `json:"meaning"`
	Description string     `json:"description"`
	ListRuby    []string   `json:"listRuby"`
	ListRubyTr  []string   `json:"listRubyTr"`
	ListKanji   []string   `json:"listKanji"`
	CreateBy    int64      `json:"-"`
	CreateDt    *time.Time `json:"-"`
	UpdateBy    int64      `json:"-"`
	UpdateDt    *time.Time `json:"-"`
}

func GetSentenceQuery() *db.QueryComposer {
	return db.Query(`SELECT sentence_id, sentence, meaning, description, list_ruby, list_ruby_tr, list_kanji, create_by, create_dt, update_by, update_dt FROM public.sentence`)
}

func (p *PublicSentence) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetSentenceQuery().
		Where().
		Int64("sentence_id", "=", p.SentenceID).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicSentence) GetBySentence(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetSentenceQuery().
		Where().
		StringEq("sentence", p.Sentence).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicSentence) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.sentence
		(sentence, meaning, description, list_ruby, list_ruby_tr, list_kanji, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
		RETURNING sentence_id;`,
		p.Sentence,
		p.Meaning,
		p.Description,
		p.ListRuby,
		p.ListRubyTr,
		p.ListKanji,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.SentenceID)

	return err
}

func (p *PublicSentence) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.sentence SET sentence = $1
		, meaning = $2
		, description = $3
		, list_ruby = $4
		, list_ruby_tr = $5
		, list_kanji = $6
		, update_by = $7
		, update_dt = $8
		WHERE sentence_id = $9`,
		p.Sentence,
		p.Meaning,
		p.Description,
		p.ListRuby,
		p.ListRubyTr,
		p.ListKanji,
		p.UpdateBy,
		p.UpdateDt,
		p.SentenceID,
	)

	return err
}

func (p *PublicSentence) Res() SentenceRes {
	res := SentenceRes{
		SentenceID:  p.SentenceID,
		Sentence:    p.Sentence,
		Meaning:     p.Meaning,
		Description: p.Description,
		ListRuby:    p.ListRuby,
		ListRubyTr:  p.ListRubyTr,
		ListKanji:   p.ListKanji,
	}

	return res
}
