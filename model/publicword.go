//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicWord struct {
	WordID       int64      `db:"word_id,pk" json:"wordId" form:"wordId" validate:"required"`
	Word         string     `db:"word,use_zero" json:"word" form:"word" validate:"required,lte=10"`
	WordTag      string     `db:"word_tag,use_zero" json:"wordTag" form:"wordTag" validate:"required,oneof='OTHER' 'JLPT_N1_VOCAB' 'JLPT_N2_VOCAB' 'JLPT_N3_VOCAB' 'JLPT_N4_VOCAB' 'JLPT_N5_VOCAB'"`
	ListWordRuby []string   `db:"list_word_ruby,array,use_zero" json:"listWordRuby" form:"listWordRuby" validate:"required"`
	ListMeaning  []string   `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" validate:"required"`
	ListKanji    []string   `db:"list_kanji,array,use_zero" json:"listKanji" form:"listKanji" validate:"required"`
	IsSingleruby bool       `db:"is_singleruby,use_zero" json:"isSingleruby" form:"isSingleruby" validate:"required"`
	IsCommon     bool       `db:"is_common,use_zero" json:"isCommon" form:"isCommon" validate:"required"`
	CreateBy     int64      `db:"create_by,use_zero" json:"createBy" form:"createBy" validate:"required"`
	CreateDt     *time.Time `db:"create_dt,use_zero" json:"createDt" form:"createDt" validate:"required"`
	UpdateBy     int64      `db:"update_by,use_zero" json:"updateBy" form:"updateBy" validate:"required"`
	UpdateDt     *time.Time `db:"update_dt,use_zero" json:"updateDt" form:"updateDt" validate:"required"`
}
