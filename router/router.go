package router

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"kanji/config"
	"kanji/constant"
	"kanji/controller"
	"kanji/cryption"
	"kanji/response"
	"kanji/swg"
	"net/http"
	"strconv"
	"time"

	echoSwagger "github.com/swaggo/echo-swagger"
)

type host struct {
	echo *echo.Echo
}

func Init() *echo.Echo {
	var err error
	var listenPort int
	hosts := make(map[string]*host)
	if listenPort, err = strconv.Atoi(config.ListenTo.Port); err != nil {
		panic(err)
	}

	web := websiteRouter()
	webDomain := config.WebDomainName
	hosts[webDomain] = &host{web}
	hosts[fmt.Sprintf("%s:%v", webDomain, listenPort)] = hosts[webDomain]

	checkToken := checkTokenMiddleware()

	authController := controller.AuthComposer()
	kanjiController := controller.KanjiComposer()
	wordController := controller.WordComposer()
	sentenceController := controller.SentenceComposer()
	wordtestController := controller.WordtestComposer()

	web.GET("/swg/*", echoSwagger.WrapHandler)

	web.GET("/", controller.Ping)
	web.POST("/sign-up", authController.SignUp)
	web.POST("/sign-in", authController.SignIn)
	web.GET("/sign-out", authController.SignOut, checkToken)
	web.GET("/me", authController.Me, checkToken)

	webKanji := web.Group("/kanji")
	webKanji.POST("", kanjiController.Page)
	webKanji.GET("/id/:kanji_id", kanjiController.GetById)
	webKanji.GET("/id/detail/:kanji_id", kanjiController.GetByIdDetail)
	webKanji.GET("/kanji/:kanji", kanjiController.GetByKanji)
	webKanji.GET("/kanji/detail/:kanji", kanjiController.GetByKanjiDetail)
	webKanji.POST("/create", kanjiController.Create, checkToken)
	webKanji.POST("/update", kanjiController.Update, checkToken)

	webWord := web.Group("/word")
	webWord.POST("", wordController.Page)
	webWord.GET("/id/:word_id", wordController.GetById)
	webWord.GET("/word/:word", wordController.GetByWord)
	webWord.GET("/id/detail/:word_id", wordController.GetByIdDetail)
	webWord.GET("/word/detail/:word", wordController.GetByWordDetail)
	webWord.POST("/create", wordController.Create, checkToken)
	webWord.POST("/update", wordController.Update, checkToken)

	webSentence := web.Group("/sentence")
	webSentence.POST("", sentenceController.Page)
	webSentence.GET("/id/:sentence_id", sentenceController.GetById)
	webSentence.POST("/create", sentenceController.Create, checkToken)
	webSentence.POST("/update", sentenceController.Update, checkToken)

	webWordtest := web.Group("/wordtest")
	webWordtest.POST("", wordtestController.Page)

	e := echo.New()
	e.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		hst := hosts[req.Host]

		if config.Environment != config.PRODUCTION {
			swg.SwaggerInfo.Title = "Website API"
			swg.SwaggerInfo.BasePath = "/"
		}

		if hst == nil {
			err = echo.ErrNotFound
		} else {
			hst.echo.ServeHTTP(res, req)
		}
		return
	})
	return e
}

func checkTokenMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cookie, err := c.Cookie(config.CookieAuthName)
			if err != nil {
				return response.ErrorForce("Akses ditolak!", response.Payload{}).SendJSON(c)
			}

			token := cookie.Value
			tokenPayload, err := cryption.DecryptAES64([]byte(token))
			if err != nil {
				return response.ErrorForce("Akses telah kadaluarsa", response.Payload{}).SendJSON(c)
			}

			if len(tokenPayload) == constant.TokenPayloadLen {
				expiredUnix := binary.BigEndian.Uint64(tokenPayload)
				expiredAt := time.Unix(int64(expiredUnix), 0)
				now := time.Now()
				if now.After(expiredAt) {
					return response.ErrorForce("Akses telah kadaluarsa!", response.Payload{}).SendJSON(c)
				} else {
					usrLogin := controller.UserLogin{
						UserID:   int64(binary.BigEndian.Uint64(tokenPayload[8:16])),
						Username: string(bytes.TrimSpace(tokenPayload[16:36])),
						Email:    string(bytes.TrimSpace(tokenPayload[36:56])),
						UserType: string(bytes.TrimSpace(tokenPayload[56:76])),
					}
					c.Set(constant.TokenUserContext, usrLogin)
					return next(c)
				}
			} else {
				return response.ErrorForce("Akses telah kadaluarsa!", response.Payload{}).SendJSON(c)
			}
		}
	}
}

func httpErrorHandler(err error, c echo.Context) {
	var errorResponse *response.ErrorResponse
	code := http.StatusInternalServerError

	switch e := err.(type) {
	case *echo.HTTPError:
		// Handle pada saat URL yang di request tidak ada. atau ada kesalahan server.
		code = e.Code
		errorResponse = &response.ErrorResponse{
			IsError: true,
			Message: strconv.Itoa(code) + " code. " + fmt.Sprintf("%v", e.Message),
			Payload: map[string]interface{}{},
		}
	case *response.ErrorResponse:
		errorResponse = e
	default:
		// Handle error dari panic
		if config.Environment != config.PRODUCTION {
			errorResponse = &response.ErrorResponse{
				IsError: true,
				Message: err.Error(),
				Payload: map[string]interface{}{},
			}
		} else {
			code = http.StatusInternalServerError
			errorResponse = &response.ErrorResponse{
				IsError: true,
				Message: "Internal server error",
				Payload: map[string]interface{}{},
			}
		}
	}

	js, err := json.Marshal(errorResponse)
	if err == nil {
		c.String(code, string(js))
	} else {
		b := []byte("{error: true, message: \"unresolved error\"}")
		c.Blob(code, echo.MIMEApplicationJSONCharsetUTF8, b)
	}
}
